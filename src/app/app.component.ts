import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { ApplyPage } from '../pages/apply/apply';
import { AdminPage } from '../pages/admin/admin';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {

      let token = localStorage.getItem('token');
      let is_admin = localStorage.getItem('is_admin');

      if(token)
      {
        if(is_admin == '1')
        this.rootPage = AdminPage
      else
        this.rootPage = ApplyPage
      }
      else
        this.rootPage = HomePage

      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}

