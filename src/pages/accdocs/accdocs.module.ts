import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AccdocsPage } from './accdocs';

@NgModule({
  declarations: [
    AccdocsPage,
  ],
  imports: [
    IonicPageModule.forChild(AccdocsPage),
  ],
})
export class AccdocsPageModule {}
