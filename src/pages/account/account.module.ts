import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AccountPage } from './account';
import { AccdocsPageModule } from '../accdocs/accdocs.module';

@NgModule({
  declarations: [
    AccountPage,
  ],
  imports: [
    IonicPageModule.forChild(AccountPage),
    AccdocsPageModule,
  ],
})
export class AccountPageModule {}
