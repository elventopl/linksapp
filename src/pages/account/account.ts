import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AccdocsPage } from '../accdocs/accdocs';
import { AuthProvider } from '../../providers/auth/auth';
import { HomePage } from '../home/home';

@IonicPage()
@Component({
  selector: 'page-account',
  templateUrl: 'account.html',
})
export class AccountPage {
    form: FormGroup;

  constructor(
  	public navCtrl: NavController, 
    private auth: AuthProvider,
    private fb: FormBuilder,
  	public navParams: NavParams) {

        this.form = this.fb.group({
            buyer_name: ['', Validators.required],
            invoice_amount: ['', [Validators.required]],
            data_currency: ['', [Validators.required]],
            data_term: ['', [Validators.required]],
            due_date: ['', [Validators.required]],
        });

  }

  next() {
var pattern =/^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$/;

    if(pattern.test(this.form.value.due_date))
    {
      console.log('valid date')
    this.navCtrl.push(AccdocsPage, {data: this.form.value})
    }
    else
    {
      console.log('invalid date')
      this.auth.presentToast('incorrect date! use dd/mm/yyyy')
    }
  }
  
    logout(){
        localStorage.clear();
        this.navCtrl.setRoot(HomePage)
    }

}
