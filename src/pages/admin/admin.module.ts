import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdminPage } from './admin';
import { SegmentPageModule } from '../segment/segment.module';
import { UsersPageModule } from '../users/users.module';

@NgModule({
  declarations: [
    AdminPage,
  ],
  imports: [
    IonicPageModule.forChild(AdminPage),
    SegmentPageModule,
    UsersPageModule
  ],
})
export class AdminPageModule {}
