import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SegmentPage } from '../segment/segment';
import { HomePage } from '../home/home';
import { AuthProvider } from '../../providers/auth/auth';
import { UsersPage } from '../users/users';

@IonicPage()
@Component({
    selector: 'page-admin',
    templateUrl: 'admin.html',
})
export class AdminPage {
    date: any = new Date();
    object: any = {
        account_receivable: ['buyer_name','data_currency','invoice_amount','status'],
        tax_return: ['buyer_name','country','invoice_amount','status'],
        company_report: ['buyer_name','country','type_report','due_date','report_upload'],
    }
    numbers: any = {};
    // object: any = {
    //     account_receivable: ['buyer_name','data_currency','invoice_amount','due_date','status'],
    //     tax_return: ['buyer_name','data_currency','invoice_amount','due_date','status'],
    //     company_report: ['buyer_name','country','type_report','due_date','report_url'],
    // }

    constructor(
        public navCtrl: NavController,
        public auth: AuthProvider,
        public navParams: NavParams) {

        this.auth.getSummary()
            .then((resp: any) => {
                console.log(resp)
                this.numbers = resp
            }, error => {
                console.log(error)
            })

    }

    segment(type) {
        console.log(type)
        this.object = {
        account_receivable: ['buyer_name','data_currency','invoice_amount','status'],
        tax_return: ['buyer_name','data_country','invoice_amount','status'],
        company_report: ['buyer_name','country','type_report','due_date','report_upload'],
    }
        this.navCtrl.push(SegmentPage, { type: type, fields: this.object[type] })
    }

    logout() {
        localStorage.clear();
        this.navCtrl.setRoot(HomePage)
    }

    openUsers(){
        this.navCtrl.push(UsersPage)
    }

    links(){
        // this.auth.uploadLink().then(resp => {
        //     console.log(resp)
        // })

    }

}