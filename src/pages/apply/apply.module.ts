import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ApplyPage } from './apply';
import { AccountPageModule } from '../account/account.module';
import { TaxPageModule } from '../tax/tax.module';
import { CompanyPageModule } from '../company/company.module';
import { UserSegmentPageModule } from '../user-segment/user-segment.module';

@NgModule({
  declarations: [
    ApplyPage,
  ],
  imports: [
    IonicPageModule.forChild(ApplyPage),
    AccountPageModule,
	TaxPageModule,
	CompanyPageModule,
	UserSegmentPageModule
  ],
})
export class ApplyPageModule {}
