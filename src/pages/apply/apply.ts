import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AccountPage } from '../account/account';
import { TaxPage } from '../tax/tax';
import { CompanyPage } from '../company/company';
import { HomePage } from '../home/home';
import { AuthProvider } from '../../providers/auth/auth';
import { AdminPage } from '../admin/admin';
import { UserSegmentPage } from '../user-segment/user-segment';

@IonicPage()
@Component({
    selector: 'page-apply',
    templateUrl: 'apply.html',
})
export class ApplyPage {
    activeButton: any;
    numbers: any = {};
    object: any = {
        account_receivable: ['buyer_name','data_currency','invoice_amount','due_date','status'],
        tax_return: ['buyer_name','data_country','invoice_amount','due_date','status'],
        company_report: ['buyer_name','country','type_report','due_date','report_url'],
    }

    constructor(
        public navCtrl: NavController, 
        public auth: AuthProvider, 
        public navParams: NavParams) {
        this.auth.getSummary()
        .then((resp:any) => {
            console.log(resp)
            this.numbers = resp
        }, error => {
            console.log(error)
        })
    }

    select(button) {
        console.log(button)
        this.navCtrl.push(button);
    }

    checkActive(button) {
        return button == this.activeButton;
    }

    logout(){
        localStorage.clear();
        this.navCtrl.setRoot(HomePage)
    }

    admin(){
        this.navCtrl.push(AdminPage)
    }

      segment(type) {
          this.object = {
        account_receivable: ['buyer_name','data_currency','invoice_amount','due_date','status'],
        tax_return: ['buyer_name','data_country','invoice_amount','due_date','status'],
        company_report: ['buyer_name','country','type_report','due_date','report_url'],
    }
          console.log(type)
        this.navCtrl.push(UserSegmentPage, {type: type, fields: this.object[type]})
      }

      open(){
          
      }
}