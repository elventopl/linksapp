import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CompanyPage } from './company';
import { CompanydocsPageModule } from '../companydocs/companydocs.module';

@NgModule({
  declarations: [
    CompanyPage,
  ],
  imports: [
    IonicPageModule.forChild(CompanyPage),
    CompanydocsPageModule
  ],
})
export class CompanyPageModule {}
