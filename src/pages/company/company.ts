import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthProvider } from '../../providers/auth/auth';
import { CompanydocsPage } from '../companydocs/companydocs';
import { ApplyPage } from '../apply/apply';

@IonicPage()
@Component({
  selector: 'page-company',
  templateUrl: 'company.html',
})
export class CompanyPage {
    form: FormGroup;

  constructor(
  	public navCtrl: NavController, 
    private auth: AuthProvider,
    private fb: FormBuilder,
  	public navParams: NavParams) {

        this.form = this.fb.group({
            buyer_name: ['', Validators.required],
            country: ['', [Validators.required]],
            address: ['', [Validators.required]],
            reg_number: [''],
            type_report: ['', [Validators.required]],
        });

  }

  // next() {
  // 	this.auth.getProfile()
  // 	.then(resp => {
  // 		console.log(resp)
  // 	})
  // 	this.navCtrl.push(CompanydocsPage, {data: this.form.value})
  // }

  next() {

  	let that = this;
  	if(this.form.valid)
  	{

  //	let form = Object.assign(this.data,this.form)


    	this.auth.submitCompany(this.form.value, 'company_report')
    	.then((resp:any) => {
    		// if( !resp.success || resp.success == 0)
    		// {
    		that.auth.presentToast('Upload Successful!')
    		that.navCtrl.setRoot(ApplyPage);
    		// }
    		// else
    		// {
    		// //that.navCtrl.pop();
    		// that.auth.presentToast('Registration Successful!')
    		// }
    		console.log(resp)
    	}, error => {
    		console.log(error)
    	})
  	}
  	else
  	{
    		that.auth.presentToast('Invalid Details!')
  	}
  }
}
