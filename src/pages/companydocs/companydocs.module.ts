import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CompanydocsPage } from './companydocs';

@NgModule({
  declarations: [
    CompanydocsPage,
  ],
  imports: [
    IonicPageModule.forChild(CompanydocsPage),
  ],
})
export class CompanydocsPageModule {}
