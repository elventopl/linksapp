import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { ApplyPage } from '../apply/apply';

@IonicPage()
@Component({
  selector: 'page-companydocs',
  templateUrl: 'companydocs.html',
})
export class CompanydocsPage {
    data: any = {};
    contract: any;
    form:any = {};
    keys: any = 0;

  constructor(
  	public navCtrl: NavController,
    private auth: AuthProvider,
  	public navParams: NavParams) {

  		 this.data = this.navParams.data.data;
  }

  upload() {

  	let that = this;
  	if(this.form)
  	{

  	let form = Object.assign(this.data,this.form)

    console.log('ionViewDidLoad AccountDocumentsPage ', form);

    	this.auth.upload(form, 'company_report')
    	.then((resp:any) => {
    		// if( !resp.success || resp.success == 0)
    		// {
    		that.auth.presentToast('Upload Successful!')
    		that.navCtrl.setRoot(ApplyPage);
    		// }
    		// else
    		// {
    		// //that.navCtrl.pop();
    		// that.auth.presentToast('Registration Successful!')
    		// }
    		console.log(resp)
    	}, error => {
    		console.log(error)
    	})
  	}
  	else
  	{
    		that.auth.presentToast('Invalid Details!')
  	}
  }

  onFileSelected(event, data) {
    console.log(data);
    this.form[data] = event.target.files[0]
    this.keys = Object.keys(this.form)
    console.log(this.form);
    //this.contract = event.target.files; 
  }

}
