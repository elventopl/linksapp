import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';

@IonicPage()
@Component({
  selector: 'page-forgot',
  templateUrl: 'forgot.html',
})
export class ForgotPage {
email_id: any;

  constructor(
  	public navCtrl: NavController, 
  	public navParams: NavParams,
    private auth: AuthProvider,
  	) {
  }

  submit() {
  	let that = this;
  	if(this.email_id)
  	{
    	this.auth.forgotPassword(this.email_id)
    	.then((resp:any) => {
    		if( !resp.status)
    		{
    		that.auth.presentToast('Invalid Email!')
    		}
    		else
    		{
    		that.navCtrl.pop();
    		that.auth.presentToast('Password sent to your email ID!')
    		}
    		console.log(resp)
    	}, error => {
    		console.log(error)
    	})
  	}
  	else
  	{
    		that.auth.presentToast('Invalid Email!')
  	}
  }

}
