import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { AuthProvider } from '../../providers/auth/auth';

@IonicPage()
@Component({
  selector: 'page-info',
  templateUrl: 'info.html',
})
export class InfoPage {
    object: any = {
        account_receivable: ['buyer_name','data_currency','invoice_amount','information','status'],
        tax_return: ['buyer_name','data_currency','invoice_amount','due_date','status'],
        company_report: ['buyer_name','country','type_report','due_date','report_url'],
    }
	record: any = {}
	type: any;
	text: any = "..."
	showText: boolean = false;

    constructor(
        public auth: AuthProvider,
        public navCtrl: NavController,
        public navParams: NavParams) {
    	this.record = navParams.data.record;
    	this.type = navParams.data.type;
    console.log('ionViewDidLoad type ',this.type);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InfoPage');
  }

Export2Doc(){
	var filename = '';
	this.showText = true;

  // document.getElementById("text").style.display  = "none";

    var preHtml = "<html xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:w='urn:schemas-microsoft-com:office:word' xmlns='http://www.w3.org/TR/REC-html40'><head><meta charset='utf-8'><title>Export HTML To Doc</title></head><body>";
    var postHtml = "</body></html>";
    var html = preHtml+document.getElementById('exportContent').innerHTML+postHtml;

    var blob = new Blob(['\ufeff', html], {
        type: 'application/msword'
    });
    
    // Specify link url
    var url = 'data:application/vnd.ms-word;charset=utf-8,' + encodeURIComponent(html);
    
    // Specify file name
    filename = filename?filename+'.doc':'document.doc';
    
    // Create download link element
    var downloadLink = document.createElement("a");

    document.body.appendChild(downloadLink);
    
    if(navigator.msSaveOrOpenBlob ){
        navigator.msSaveOrOpenBlob(blob, filename);
    }else{
        // Create a link to the file
        downloadLink.href = url;
        
        // Setting the file name
        downloadLink.download = filename;
        
        //triggering the function
        downloadLink.click();
    }
    
    document.body.removeChild(downloadLink);
}


}
