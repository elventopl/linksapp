import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthProvider } from '../../providers/auth/auth';
import { ApplyPage } from '../apply/apply';
import { ForgotPage } from '../forgot/forgot';
import { AdminPage } from '../admin/admin';

@IonicPage()
@Component({
    selector: 'page-login',
    templateUrl: 'login.html',
})
export class LoginPage {
    form: FormGroup;

    constructor(
        public navCtrl: NavController,
        private fb: FormBuilder,
        private loadingCtrl: LoadingController,
        private auth: AuthProvider,
        public navParams: NavParams) {

        this.form = this.fb.group({
            email_id: ['', Validators.required],
            password: ['', [Validators.required]],
            quote: ['', [Validators.required]],
        });
    }

    getQuote() {
        // const loader = this.loadingCtrl.create({
        //   content: "Please wait...",
        //   duration: 3000
        // });
        // loader.present();

        this.auth.getQuote(this.form.value)
            .then((resp: any) => {
                //  loader.dismiss();
                if (resp.message_error) {

                } else {

                }
            }, error => {
                // loader.dismiss();
            })
    }

    login() {
        const loader = this.loadingCtrl.create({
            content: "Please wait...",
            duration: 3000
        });
        loader.present();

        if (this.form.valid) {
            this.auth.login(this.form.value)
                .then((resp: any) => {
                    // console.log(resp)
                    loader.dismiss();
                    if (resp.message_error) {

                    } else {
                        localStorage.setItem('token', resp.token);
                        localStorage.setItem('comp_name', resp.comp_name);
                        localStorage.setItem('email_id', resp.email_id);
                        localStorage.setItem('is_admin', resp.is_admin);
                        localStorage.setItem('user_id', resp.user_id);
                        localStorage.setItem('is_logged_in', resp.is_logged_in);


                      let is_admin = localStorage.getItem('is_admin');

                        if(is_admin == '1')
                        this.navCtrl.setRoot(AdminPage);
                      else
                        this.navCtrl.setRoot(ApplyPage);


                        // this.navCtrl.setRoot(ApplyPage)
                    }
                }, error => {
                    loader.dismiss();
                    console.log(error)
                })
        }

    }

    forgot() {
        this.navCtrl.push(ForgotPage)
    }

}