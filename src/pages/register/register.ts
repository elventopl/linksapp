import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthProvider } from '../../providers/auth/auth';

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
    form: FormGroup;

  constructor(
  	public navCtrl: NavController, 
    private fb: FormBuilder,
    private auth: AuthProvider,
  	public navParams: NavParams) {
        this.form = this.fb.group({
            comp_name: ['', Validators.required],
            comp_mob: ['', [Validators.required]],
            email_id: ['', [Validators.required]],
            password: ['', [Validators.required]],
            password2: ['', [Validators.required]],
        });
  }

  ionViewDidLoad() {
  }

  submit(){
  	let that = this;
  	if(this.form.valid)
  	{
    	this.auth.register(this.form.value)
    	.then((resp:any) => {
    		if( !resp.success || resp.success == 0)
    		{
    		that.auth.presentToast('Invalid Details!')
    		}
    		else
    		{
    		that.navCtrl.pop();
    		that.auth.presentToast('Registration Successful!')
    		}
    		console.log(resp)
    	}, error => {
    		console.log(error)
    	})
  	}
  	else
  	{
    		that.auth.presentToast('Invalid Details!')
  	}
  }

}
