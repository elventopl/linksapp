import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SegmentPage } from './segment';
import { InfoPageModule } from '../info/info.module';

@NgModule({
  declarations: [
    SegmentPage,
  ],
  imports: [
    IonicPageModule.forChild(SegmentPage),
    InfoPageModule
  ],
})
export class SegmentPageModule {}
