import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ModalController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { AuthProvider } from '../../providers/auth/auth';
import { InfoPage } from '../info/info';

@IonicPage()
@Component({
    selector: 'page-segment',
    templateUrl: 'segment.html',
})
export class SegmentPage {
    tab: any = 'application'
    type: any;
    fields: any;
    realData: any = [];
    data: any = [];
    definition: any = {};

    constructor(
        public auth: AuthProvider,
        public navCtrl: NavController,
        public alertCtrl: AlertController,
        public modalCtrl: ModalController,
        public navParams: NavParams) {

        this.definition = { buyer_name: '买家 Buyer', report_upload: 'Upload Report', data_currency: '货币 Currency', invoice_amount: '金额 Amount', status: '通过/不通过/待批/Approved/Rejected/Pending', due_date: '日期  Date', data_country: '国家  Country', country: '国家  Country', type_report: '种类  Type', report_url: '报告  Report', information: '资料 Information' };

        this.type = navParams.data.type;
        this.fields = navParams.data.fields;

        if (this.type == 'company_report')
            this.definition.buyer_name = '公司名称  Company Name'
    }

    ionViewDidLoad() {
        this.auth.getApplicationData(this.type)
            .then(resp => {
                this.realData = resp;
                this.data = resp;
                console.log(resp)
            })
    }

    showAction (record, index) {
        let info = this.modalCtrl.create(InfoPage, {record: record,type: this.type});
        info.onDidDismiss(data => {
            //console.log(data);
        });
        info.present();
    }

    showConfirm(record, index) {
        let that = this;
        console.log("showConfirm type ", this.type)
        if(this.tab == 'application')
        {
        const confirm = this.alertCtrl.create({
            title: 'Action',
            cssClass: 'alertbox',
            // message: 'Do you agree to use this lightsaber to do good across the intergalactic galaxy?',
            buttons: [{
                    text: 'Approved',
                    handler: () => {
                           that.data[index].status = 'Approved';
                        console.log('Accept clicked', record);
                       that.auth.updateRecord(record.id, that.type, 'Approved')
                       .then(resp => {
                           record.status = 'Approved';
                           console.log(resp)
                       })
                    }
                },
                {
                    text: 'Rejected',
                    handler: () => {
                           that.data[index].status = 'Rejected';
                        console.log('Reject clicked', record);
                       that.auth.updateRecord(record.id, that.type, 'Rejected')
                       .then(resp => {
                           record.status = 'Rejected';
                           console.log(resp)
                       })                        
                    }
                },
                {
                    text: 'Pending',
                    handler: () => {
                           that.data[index].status = 'Pending';
                        console.log('Reject clicked', record);
                       that.auth.updateRecord(record.id, that.type, 'Pending')
                       .then(resp => {
                           record.status = 'Pending';
                           console.log(resp)
                       })                        
                    }
                },
                {
                    text: 'Cancel',
                    handler: () => {
                        console.log('Reject clicked');
                    }
                }
            ]
        });
        confirm.present();   
        }
        else
        {
        let info = this.modalCtrl.create(InfoPage, {record: record,type: this.type});
        info.onDidDismiss(data => {
            //console.log(data);
        });
        info.present();
        }

    }

    uploadReport(event,d,i){
        let report = event.target.files[0]
        let that = this;
        // this.auth.uploadReport(report,d)
        this.auth.uploadReport(report,d)
        .then((resp:any) => {
            // if( !resp.success || resp.success == 0)
            // {
            that.auth.presentToast('Upload Successful!')
            //that.navCtrl.setRoot(ApplyPage);
            // }
            // else
            // {
            // //that.navCtrl.pop();
            // that.auth.presentToast('Registration Successful!')
            // }
            console.log(resp)
        }, error => {
            console.log(error)
        })        
    }


    onSegmentChange(tab) {
        console.log("onSegmentChange ",tab);
        console.log("type ",this.type);
        if(this.type == 'account_receivable')
        {
            if(tab == 'product')
            {
                this.fields.splice(3,1)
                this.fields.splice(3,0,'due_date', 'information')   
            }
            else
            {
                this.fields.splice(3,2)
                this.fields.splice(3,0,'status')  
            }
        }
        
        if(this.type == 'tax_return')
        {
            if(tab == 'product')
            {
                this.fields.splice(3,1)
                this.fields.splice(3,0,'information')   
                this.fields.splice(3,0,'due_date')   
            }
            else
            {  
                this.fields.splice(3,1)
                this.fields.splice(3,1)
                this.fields.splice(3,0,'status')  
            }
        }

        if(this.type == 'company_report')
        {
            if(tab == 'product')
            {
                this.fields.splice(4,1)
            }
            else
            {
                this.fields.splice(4,0,'status')     
            }
        }

    }

    select(records) {
        console.log(records);
        if(records == 'all')
        {
            this.data = this.realData;
        }
        else
        {
            this.data = this.realData.filter(function(data){
                return data.status == 'Approved';
            })
        }
    }

    logout() {
        localStorage.clear();
        this.navCtrl.setRoot(HomePage)
    }

}