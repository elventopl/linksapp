import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TaxPage } from './tax';
import { TaxdocsPageModule } from '../taxdocs/taxdocs.module';

@NgModule({
  declarations: [
    TaxPage,
  ],
  imports: [
    IonicPageModule.forChild(TaxPage),
    TaxdocsPageModule,
  ],
})
export class TaxPageModule {}
