import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthProvider } from '../../providers/auth/auth';
import { TaxdocsPage } from '../taxdocs/taxdocs';
import { HomePage } from '../home/home';
import * as $ from 'jquery';

@IonicPage()
@Component({
  selector: 'page-tax',
  templateUrl: 'tax.html',
})
export class TaxPage {
    form: FormGroup;

  constructor(
  	public navCtrl: NavController, 
    private auth: AuthProvider,
    private fb: FormBuilder,
  	public navParams: NavParams) {

        this.form = this.fb.group({
            buyer_name: ['', Validators.required],
            invoice_amount: ['', [Validators.required]],
            data_country: ['', [Validators.required]],
            data_term: ['', [Validators.required]],
            due_date: ['', [Validators.required]],
        });

    $('inputDate').on('onkeydown', function (e) {
      var leng = $(this).val().length;
      var code;
      if (window.event) {
        code = e.keyCode;
      }else {
        code = e.which;
      };

      var allowedCharacters = [49,50,51,52,53,54,55,56,57,48,45];
      var isValidInput = false;

      for (var i = allowedCharacters.length - 1; i >= 0; i--) {
        if(allowedCharacters[i] == code){
          isValidInput = true;
        } 
      };

      if(isValidInput === false || /* Can only input 1,2,3,4,5,6,7,8,9 or - */
       (code == 45 && (leng < 2 || leng > 5 || leng == 3 || leng == 4)) ||
       ((leng == 2 || leng == 5) && code !== 45) || /* only can hit a - for 3rd pos. */
        leng == 10 ) /* only want 10 characters "12-45-7890" */
      {

        event.preventDefault();
        return;
      }

    });


  }

  next() {
var pattern =/^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$/;

    if(pattern.test(this.form.value.due_date))
    {
      console.log('valid date')
    this.navCtrl.push(TaxdocsPage, {data: this.form.value})
    }
    else
    {
      console.log('invalid date')
      this.auth.presentToast('incorrect date! use dd/mm/yyyy')
    }
  }

    logout(){
        localStorage.clear();
        this.navCtrl.setRoot(HomePage)
    }

}
