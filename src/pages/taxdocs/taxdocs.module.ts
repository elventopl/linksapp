import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TaxdocsPage } from './taxdocs';

@NgModule({
  declarations: [
    TaxdocsPage,
  ],
  imports: [
    IonicPageModule.forChild(TaxdocsPage),
  ],
})
export class TaxdocsPageModule {}
