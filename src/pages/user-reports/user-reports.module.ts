import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserReportsPage } from './user-reports';

@NgModule({
  declarations: [
    UserReportsPage,
  ],
  imports: [
    IonicPageModule.forChild(UserReportsPage),
  ],
})
export class UserReportsPageModule {}
