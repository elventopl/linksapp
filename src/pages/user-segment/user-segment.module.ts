import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserSegmentPage } from './user-segment';

@NgModule({
  declarations: [
    UserSegmentPage,
  ],
  imports: [
    IonicPageModule.forChild(UserSegmentPage),
  ],
})
export class UserSegmentPageModule {}
