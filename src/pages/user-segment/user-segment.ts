import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { HomePage } from '../home/home';

@IonicPage()
@Component({
    selector: 'page-user-segment',
    templateUrl: 'user-segment.html',
})
export class UserSegmentPage {
    type: any;
    tab: any = 'account_receivable';
    fields: any;
    data: any = [];
    definition: any = {};
    object: any = {
        account_receivable: ['buyer_name', 'data_currency', 'invoice_amount', 'due_date', 'status'],
        tax_return: ['buyer_name', 'data_country', 'invoice_amount', 'due_date', 'status'],
        company_report: ['buyer_name', 'country', 'type_report', 'due_date', 'report_url'],
    }
    account_receivable: any = [];
    tax_return: any = [];
    company_report: any = [];

    constructor(
        public auth: AuthProvider,
        public navCtrl: NavController,
        public navParams: NavParams) {
        
        this.definition = { buyer_name: '买家 Buyer', data_currency: '货币 Currency', invoice_amount: '金额 Amount', status: '通过/不通过/待批/Approved/Rejected/Pending', due_date: '日期  Date', country: '国家  Country', data_country: '国家  Country', type_report: '种类  Type', report_url: '报告  Report' };
        this.type = navParams.data.type;
        this.fields = navParams.data.fields;
        if (this.type == 'company_report')
            this.definition.buyer_name = '公司名称  Company Name'
    }

    ionViewDidLoad() {
        this.auth.getApplicationData('account_receivable')
            .then(resp => {
                this.account_receivable = resp;
                this.data = resp;
            })
        this.auth.getApplicationData('tax_return')
            .then(resp => {
                this.tax_return = resp;
            })
        this.auth.getApplicationData('company_report')
            .then(resp => {
                this.company_report = resp;
            })
    }

    logout() {
        localStorage.clear();
        this.navCtrl.setRoot(HomePage)
    }

        onSegmentChange(tab) {
        this.data = this[tab]
        console.log(this[tab]);
        // if(this.type == 'account_receivable')
        // {
        //     if(tab == 'product')
        //     {
        //         this.fields.splice(4,1)
        //         this.fields.splice(3,0,'type_report')  
        //     }
        //     else
        //     {
        //         this.fields.splice(3,1)
        //         this.fields.splice(4,0,'status')  
        //     }
        // }
        // else
        // {
        //     if(tab == 'product')
        //     {
        //         this.fields.splice(4,1)
        //     }
        //     else
        //     {
        //         this.fields.splice(4,0,'status')     
        //     }
        // }

    }

}