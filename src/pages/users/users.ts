import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { AuthProvider } from '../../providers/auth/auth';

@IonicPage()
@Component({
  selector: 'page-users',
  templateUrl: 'users.html',
})
export class UsersPage {
users: any = []

  constructor(
  	public navCtrl: NavController, 
	public auth: AuthProvider,
  	public navParams: NavParams) {
  }

  ionViewDidLoad() {
        this.auth.getUsers()
            .then(resp => {
                console.log(resp)
                this.users = resp;
            })
  }

    logout(){
        localStorage.clear();
        this.navCtrl.setRoot(HomePage)
    }

}
