import { Http, Headers, RequestOptions } from '@angular/http';
import { ToastController } from 'ionic-angular';
import { Injectable } from '@angular/core';
import * as Rx from "rxjs";
import { map } from 'rxjs/operators';

@Injectable()
export class AuthProvider {

public apiUrl: any = 'http://linksfactoring.ignitershub.com/customer/api/1'
public apiAppUrl: any = 'http://linksfactoring.ignitershub.com/customer/api/1/application/'

  constructor(
  	public http: Http,
	public toastCtrl: ToastController,
  	) {
    console.log('Hello AuthProvider Provider');
  }

  login(form){
        let that = this
        return new Promise((resolve, reject) => {
		  	this.http.post(this.apiUrl+'/user/auth', form)
		  	.subscribe((resp:any) => {
		  		resolve(JSON.parse(resp._body)[0]);
		  	}, error => {
		  		console.log("error ",error)
		  		reject(error);
		  	})
        });
  }

  register(form){
        let that = this

        let headers=new Headers({
        	'Content-Type': 'multipart/form-data;', 
        });
   		 let options = new RequestOptions({ headers: headers });

        return new Promise((resolve, reject) => {
		  	this.http.post(this.apiUrl+'/user/register', form)
		  	.subscribe((resp:any) => {
		  		resolve(JSON.parse(resp._body)[0]);
		  		console.log("resp ",resp)
		  		resolve(resp);
		  	}, error => {
		  		console.log("error ",error)
		  		reject(error);
		  	})
        });
  }

  getQuote(form){
        let that = this
        this.presentToast('Quote sent on your email Id!')
		  		let object = {email_id: form.email_id, password: form.password}
		  		console.log("object ",object)
        return new Promise((resolve, reject) => {
		  	this.http.post(this.apiUrl+'/user/quote', object)
		  	.subscribe((resp:any) => {
		  		resolve(JSON.parse(resp._body)[0]);
		  		console.log("resp ",resp)
		  		resolve(resp);
		  	}, error => {
		  		console.log("error ",error)
		  		reject(error);
		  	})
        });
  }

  forgotPassword(email_id){
        let that = this
		  		let object = {email_id: email_id}
        return new Promise((resolve, reject) => {
		  	this.http.post(this.apiUrl+'/user/forgot', object)
		  	.subscribe((resp:any) => {
		  		resolve(JSON.parse(resp._body)[0]);
		  		console.log("resp ",resp)
		  		resolve(resp);
		  	}, error => {
		  		console.log("error ",error)
		  		reject(error);
		  	})
        });
  }

  getUsers(){
        let that = this
        let headers = new Headers();

        headers.append('authorization', localStorage.getItem('token'));
        headers.append('user_id', localStorage.getItem('user_id'));
        let options = new RequestOptions({ headers: headers });

        return new Promise((resolve, reject) => {
        this.http.post(this.apiAppUrl+'register_user', {}, options)
        .subscribe((resp:any) => {
          resolve(JSON.parse(resp._body));
          console.log("resp ",resp)
          resolve(resp);
        }, error => {
          console.log("error ",error)
          reject(error);
        })
        });
  }

  upload(form, path) {
        let that = this
        let formData:FormData = new FormData();
        let keys = Object.keys(form);
        for(let i=0; i< keys.length;i++)
        	formData.append(keys[i], form[keys[i]])

        let headers = new Headers();
        // headers.append('Content-Type', 'multipart/form-data');
        // headers.append('Accept', 'application/json');

		  		console.log("localStorage.getItem('token')  ",localStorage.getItem('token'))

        headers.append('authorization', localStorage.getItem('token'));
        headers.append('user_id', localStorage.getItem('user_id'));
        // headers.append('content-type', 'multipart/form-data');
        headers.append('enctype', 'multipart/form-data');
        headers.append('Accept', 'application/json');
        let options = new RequestOptions({ headers: headers });

        return new Promise((resolve, reject) => {
		  	this.http.post(this.apiUrl+'/data/'+path, formData, options)
		  	.subscribe((resp:any) => {
		  		resolve(JSON.parse(resp._body)[0]);
		  		console.log("resp ",resp)
		  		resolve(resp);
		  	}, error => {
		  		console.log("error ",error)
		  		reject(error);
		  	})
        });
  }

  uploadReport(report, d) {
    console.log("report ", d)
        let that = this
        let formData:FormData = new FormData();
          formData.append('id', d.id)
          formData.append('company_report', report)



 // console.log('uploadReport ', formData.getAll('id')); 

        let headers = new Headers();

        headers.append('authorization', localStorage.getItem('token'));
        headers.append('user_id', localStorage.getItem('user_id'));
        // headers.append('content-type', 'application/json');
        headers.append('enctype', 'multipart/form-data');
        headers.append('Accept', 'application/json');
        let options = new RequestOptions({ headers: headers });

        return new Promise((resolve, reject) => {


        that.http.post(that.apiUrl+'/data/report_update', formData, options)
        .subscribe((resp:any) => {
          resolve(resp);
          console.log("resp ",resp)
          resolve(resp);
        }, error => {
          console.log("error ",error)
          reject(error);
        })


   // var reader = new FileReader();
   // reader.readAsDataURL(report);
   // reader.onload = function () {
   //   console.log(reader.result);


   // };
   // reader.onerror = function (error) {
   //   console.log('Error: ', error);
   // };






        });
  }

  uploadLink(report) {
        let that = this
        let formData:FormData = new FormData();
         formData.append('id', '22')
         formData.append('company_report', report)



 // console.log('uploadReport ', formData.getAll('id')); 

        let headers = new Headers();

        headers.append('authorization', localStorage.getItem('token'));
        headers.append('user_id', localStorage.getItem('user_id'));
        headers.append('enctype', 'multipart/form-data');
        headers.append('Accept', 'application/json');
        let options = new RequestOptions({ headers: headers });

        return new Promise((resolve, reject) => {
        that.http.post('http://localhost/links.php', formData, options)
        .subscribe((resp:any) => {
          resolve(resp);
          console.log("resp ",resp)
          resolve(resp);
        }, error => {
          console.log("error ",error)
          reject(error);
        })
        });
  }

  submitCompany(form, path) {
        let that = this
        let formData:FormData = new FormData();
        let keys = Object.keys(form);
        for(let i=0; i< keys.length;i++)
        	formData.append(keys[i], form[keys[i]])

        let headers = new Headers();
        // headers.append('Content-Type', 'multipart/form-data');
        // headers.append('Accept', 'application/json');

    console.log('form submitCompany ', form);
		  		// console.log("localStorage.getItem('token')  ",localStorage.getItem('token'))

        headers.append('authorization', localStorage.getItem('token'));
        headers.append('user_id', localStorage.getItem('user_id'));
        headers.append('content-type', 'application/json');
        let options = new RequestOptions({ headers: headers });

        return new Promise((resolve, reject) => {
		  	this.http.post(this.apiUrl+'/data/'+path, form, options)
		  	.subscribe((resp:any) => {
		  		resolve(JSON.parse(resp._body)[0]);
		  		console.log("resp ",resp)
		  		resolve(resp);
		  	}, error => {
		  		console.log("error ",error)
		  		reject(error);
		  	})
        });
  }

  getData(data){
        let that = this
        let headers = new Headers();

        headers.append('authorization', localStorage.getItem('token'));
        headers.append('user_id', localStorage.getItem('user_id'));
        let options = new RequestOptions({ headers: headers });

        return new Promise((resolve, reject) => {
		  	this.http.post(this.apiUrl+data, {}, options)
		  	.subscribe((resp:any) => {
		  		resolve(JSON.parse(resp._body)[0]);
		  		console.log("resp ",resp)
		  		resolve(resp);
		  	}, error => {
		  		console.log("error ",error)
		  		reject(error);
		  	})
        });
  }

  getSummary(){
        let that = this
        let headers = new Headers();

        headers.append('authorization', localStorage.getItem('token'));
        headers.append('user_id', localStorage.getItem('user_id'));
        let options = new RequestOptions({ headers: headers });

        return new Promise((resolve, reject) => {
		  	this.http.post(this.apiAppUrl+'summary', {}, options)
		  	.subscribe((resp:any) => {
		  		resolve(JSON.parse(resp._body)[0]);
		  		console.log("resp ",resp)
		  		resolve(resp);
		  	}, error => {
		  		console.log("error ",error)
		  		reject(error);
		  	})
        });
  }

  getApplicationData(data){
        let that = this
        let headers = new Headers();

        headers.append('authorization', localStorage.getItem('token'));
        headers.append('user_id', localStorage.getItem('user_id'));
        let options = new RequestOptions({ headers: headers });

        return new Promise((resolve, reject) => {
		  	this.http.post(this.apiAppUrl+data, {}, options)
		  	.subscribe((resp:any) => {
		  		resolve(JSON.parse(resp._body));
		  		console.log("resp ",resp)
		  		resolve(resp);
		  	}, error => {
		  		console.log("error ",error)
		  		reject(error);
		  	})
        });
  }

  getProfile() {
        let that = this
        let headers = new Headers();
        headers.append('authorization', localStorage.getItem('token'));
        headers.append('user_id', localStorage.getItem('user_id'));
        let options = new RequestOptions({ headers: headers });

        return new Promise((resolve, reject) => {
		  	this.http.post(this.apiUrl+'/user/profile', {}, options)
		  	.subscribe((resp:any) => {
		  		resolve(JSON.parse(resp._body)[0]);
		  		console.log("resp ",resp)
		  		resolve(resp);
		  	}, error => {
		  		console.log("error ",error)
		  		reject(error);
		  	})
        });
  }

  updateRecord(id,type,status) {
        let that = this
        let headers = new Headers();
        headers.append('authorization', localStorage.getItem('token'));
        headers.append('user_id', localStorage.getItem('user_id'));
        let options = new RequestOptions({ headers: headers });

        return new Promise((resolve, reject) => {
		  	this.http.post(this.apiAppUrl+'update_report', {id: id, type: type, status: status}, options)
		  	.subscribe((resp:any) => {
		  		resolve(JSON.parse(resp._body)[0]);
		  		console.log("resp ",resp)
		  		resolve(resp);
		  	}, error => {
		  		console.log("error ",error)
		  		reject(error);
		  	})
        });
  }

  presentToast(message) {
    const toast = this.toastCtrl.create({
      message: message,
      position: 'bottom',
      duration: 3000
    });
    toast.present();
  }

}
